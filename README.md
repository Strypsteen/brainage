# Brain age prediction from EEG with MSFBCNN

## About

This project is a Pytorch implementation of our submission for the Brain Age Prediction Challenge from EEG (https://codalab.lisn.upsaclay.fr/competitions/8336). We employ a multiscale parallel filter bank CNN (MSFBCNN) [1] to extract spectral log power features which are then used to perform regression with fully connected layers. We train two separate models, one for data where the subjects' eyes are closed and one where the subjects'eyes are opened and then integrate these in a larger model combining the two predictions.

## Usage

To run the code, create an Anaconda environment using the included _environment.yml_ file:
`conda env create -f environment.yml `
Then, update the location of the training and test data in _main_brainage.py_ with the train_path and prediction_path fields respectively. Training of the models can then be performed by running the main_brainage.py file.

## Method

### Data preparation

The training data is split in 80% training and  20% validation data and then preprocessed. Firstly, a number of bad channels based on visual inspection. Then, the data is notch filterd at 60,120 and 180 Hz to remove powerline noise,bandpass filtered between 2 and 50 Hz and resampled to 100Hz. For the "eyes closed" (EC) data, a window of 38 seconds is extracted for each file, while a window of 19 seconds is employed for the shorter "eyes open" (EO) files. Finally, each channel of each window is normalized such that its mean is 0 and variance 1 in the temporal dimension.

### Model design
We employ an MSFBCNN model, designed to extract spectral log power features and classify them with two fully connected layers. We train a separate model for the EC and EO data and then combine these two in a larger model. The combined model extracts a soft attention mask from the EC and EO power features and uses this to compute a weighted average of the predictions of the two models.

The models are trained by extracting random 10-second crops from the data windows, which is further augmented by adding a small amount of Gaussian noise during training. During validation only, predictions are performed for 4 10-second crops, each with 25% overlap, and the final prediction is an average of these 4 predictions to improve robustness of the model. Training happens with an Adam optimizer. The combined model is initialized with the weights of the pre-trained separate models, which are finetuned during training with a lower learning rate alongside the layers learning the soft attention.


## References
 
Wu, H., Niu, Y., Li, F., Li, Y., Fu, B., Shi, G., & Dong, M. (2019). A parallel multiscale filter bank convolutional neural networks for motor imagery EEG classification. Frontiers in neuroscience, 13, 1275.
