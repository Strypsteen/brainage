import mne
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
mne.viz.set_browser_backend('matplotlib')
import math
import torch
import torch.nn as nn
from torch.nn import Parameter
import torch.nn.functional as F
import os
from torchsummary import summary

from models import init_weights,MSFBCNN,Merge_MSFBCNN
from loader import EC_EO_loader,joint_EC_EO_loader,predictionloader

#Path for training set and new prediction set
train_path = "/home/thomas/Documents/Python/Brainage/training/"
prediction_path = "/home/thomas/Documents/Python/Brainage/validation-final/"

#Random seed for torch
seed=1
torch.manual_seed(seed)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

dpath = os.path.abspath('')

#Path for saving trained models
model_save_path_EO = os.path.join(dpath,'Model_EO_seeded_analysis.pt')
model_save_path_EC = os.path.join(dpath,'Model_EC_seeded_analysis.pt')
model_save_path_merged = os.path.join(dpath,'Model_merged_seeded_analysis.pt')
checkpoint_path = os.path.join(dpath,'Checkpoint.pt')

#Dataloader parameters
train_split=0.8
val_split=0.2
batch_size=64


def main():

	global enable_cuda

	#Load separate EC and EO data
	print('Loading EC and EO data')
	train_loader_EC,val_loader_EC,train_loader_EO,val_loader_EO,datasize=EC_EO_loader(train_path,train_split,val_split,batch_size)
	N,M,T=datasize

	# Perform EC model training

	lr=0.01
	epochs=100
	enable_cuda=torch.cuda.is_available()
	weight_decay=1e-2
	gradacc=1
	verbose=True
	stop_delta=0.001
	patience=30


	torch.manual_seed(seed)
	torch.backends.cudnn.deterministic = True
	torch.backends.cudnn.benchmark = False


	model=MSFBCNN(N,M,T,FT=4,FS=4)
	summary(model, (N, M, T))
	if(enable_cuda):
		model.cuda()

	optimizer = torch.optim.Adam(model.parameters(),lr=lr)    
	prev_val_loss = 100
	patience_timer = 0
	early_stop = False
	epoch = 0

	while epoch in range(epochs) and (not early_stop):

		#Perform training step
		train(train_loader_EC, model, loss_function, optimizer,epoch,weight_decay,gradacc,verbose)
		val_loss = validate(train_loader_EC,val_loader_EC,model,val_loss_function,epoch,weight_decay,verbose)

		if(val_loss>prev_val_loss-stop_delta):
			patience_timer+=1
			if(verbose):
				print('Early stopping timer ', patience_timer)
			if(patience_timer == patience):
				early_stop = True
		else:
			patience_timer=0
			torch.save(model.state_dict(),checkpoint_path)
			prev_val_loss = val_loss

		epoch+=1

	if(verbose):
		print('Training EC finished')

	model.load_state_dict(torch.load(checkpoint_path))
	torch.save(model.state_dict(), model_save_path_EC)

	#Evaluate model
	val_loss = validate(train_loader_EC,val_loader_EC,model,val_loss_function,epoch,weight_decay,verbose)
	print('EC validation loss: ', val_loss)


	# Perform EO model training

	lr=0.01
	epochs=100
	enable_cuda=torch.cuda.is_available()
	weight_decay=1e-2
	gradacc=1
	verbose=True
	stop_delta=0.001
	patience=30

	torch.manual_seed(seed)
	torch.backends.cudnn.deterministic = True
	torch.backends.cudnn.benchmark = False

	model=MSFBCNN(N,M,T,FT=4,FS=4)
	summary(model, (N, M, T))

	if(enable_cuda):
		model.cuda()

	optimizer = torch.optim.Adam(model.parameters(),lr=lr)

	prev_val_loss = 100
	patience_timer = 0
	early_stop = False
	epoch = 0

	while epoch in range(epochs) and (not early_stop):

		#Perform training step
		train(train_loader_EO, model, loss_function, optimizer,epoch,weight_decay,gradacc,verbose)
		val_loss = validate(train_loader_EO,val_loader_EO,model,val_loss_function,epoch,weight_decay,verbose)

		if(val_loss>prev_val_loss-stop_delta):
			patience_timer+=1
			if(verbose):
				print('Early stopping timer ', patience_timer)
			if(patience_timer == patience):
				early_stop = True
		else:
			patience_timer=0
			torch.save(model.state_dict(),checkpoint_path)
			prev_val_loss = val_loss

		epoch+=1

	if(verbose):
		print('Training EO finished')

	model.load_state_dict(torch.load(checkpoint_path))
	torch.save(model.state_dict(), model_save_path_EO)

	#Evaluate model
	val_loss = validate(train_loader_EO,val_loader_EO,model,val_loss_function,epoch,weight_decay,verbose)
	print('EO validation loss: ', val_loss)


	# Load joint EC and EO data

	print('Loading joint EC and EO data')
	train_loader,val_loader,datasize=joint_EC_EO_loader(train_path,train_split,val_split,batch_size)
	N,M,T=datasize

	# Train combined EC and EO model

	pretrain_lr=0.0001
	lr=0.001
	epochs=100
	enable_cuda=torch.cuda.is_available()
	weight_decay=1e-2
	gradacc=16
	verbose=True
	stop_delta=0.001
	patience=30

	torch.manual_seed(seed)
	torch.backends.cudnn.deterministic = True
	torch.backends.cudnn.benchmark = False


	model=Merge_MSFBCNN(N,M,T,FT=4,FS=4)
	summary(model, ((N, M, T),(N, M, T)))

	if(enable_cuda):
		model.cuda()


	model.model1.load_state_dict(torch.load(model_save_path_EC))
	model.model2.load_state_dict(torch.load(model_save_path_EO))

	optimizer = torch.optim.Adam([{'params': model.model1.parameters()},
								  {'params': model.model2.parameters()},
								  {'params': model.fc1.parameters(), 'lr': lr},
								  {'params': model.fc2.parameters(), 'lr': lr}], lr=pretrain_lr)
	prev_val_loss = 100
	patience_timer = 0
	early_stop = False
	epoch = 0

	while epoch in range(epochs) and (not early_stop):

		#Perform training step
		train_merge(train_loader, model, loss_function, optimizer,epoch,weight_decay,gradacc,verbose)
		val_loss = validate_merge(train_loader,val_loader,model,val_loss_function,epoch,weight_decay,verbose)

		if(val_loss>prev_val_loss-stop_delta):
			patience_timer+=1
			if(verbose):
				print('Early stopping timer ', patience_timer)
			if(patience_timer == patience):
				early_stop = True
		else:
			patience_timer=0
			torch.save(model.state_dict(),checkpoint_path)
			prev_val_loss = val_loss

		epoch+=1

	if(verbose):
		print('Training merged finished')

	model.load_state_dict(torch.load(checkpoint_path))
	torch.save(model.state_dict(), model_save_path_merged)

	## Model evaluation

	val_loss = validate_merge(train_loader,val_loader,model,val_loss_function,epoch,weight_decay,verbose)
	print('Merged model validation loss', val_loss)



	#Perform prediction on new test set

	test_loader_pred=predictionloader(prediction_path,batch_size)
	predictions=predict(test_loader_pred,model,verbose)


	y_pred= [predictions[i,0] for i in range(predictions.shape[0])]
	dummy_submission = []
	test_subj=400
	for subj, pred in zip(range(1601, 1601 + test_subj), y_pred):
		dummy_submission.append({"id": subj, "age": pred})
	pd.DataFrame(dummy_submission).to_csv("mysubmission_git.csv", index=False)


#MSE Loss function for training
def loss_function(output,target,model,weight_decay):
	l = nn.MSELoss()
	sup_loss = l(output,target)
	reg = model.regularizer(weight_decay)

	return sup_loss,reg

#L1 loss function for validation
def val_loss_function(output,target,model,weight_decay):
	l = nn.L1Loss()
	sup_loss = l(output,target)
	reg = model.regularizer(weight_decay)
	
	return sup_loss,reg


#Perform 1 training step for EC/EO model
def train(train_loader, model, loss_function, optimizer, epoch, weight_decay,gradacc,verbose):

	global enable_cuda

	model.train()

	for i, (data, labels) in enumerate(train_loader):

		if(enable_cuda):
			data= data.cuda()
			labels = labels.cuda()

		if(i==0):
			running_loss = 0.0
			running_reg = 0.0
			running_sup_loss = 0.0

		output = model(data)

		sup_loss,reg = loss_function(output, labels, model,weight_decay)
		sup_loss,reg=sup_loss/gradacc,reg/gradacc
		loss = sup_loss + reg

		loss.backward()

		#Perform gradient accumulation
		if((i+1)%gradacc ==0):
			optimizer.step()
			optimizer.zero_grad()

		# print statistics
		running_loss += loss.item()
		running_reg += reg.item()
		running_sup_loss += sup_loss.item()
		N = len(train_loader)
		if(i==N-1):
			if(verbose):
				print('[%d, %5d] loss: %.3f supervised loss: %.3f regularization loss %.3f'%
						(epoch + 1, i + 1, running_loss / N, running_sup_loss/N, running_reg/N))
			running_loss = 0.0
			running_reg = 0.0
			running_sup_loss = 0.0

#Perform validation for EC/EO model
def validate(train_loader, val_loader,model,loss_function,epoch,weight_decay,verbose):

	global enable_cuda

	with torch.no_grad():
		model.eval()
		
		for i, (data, labels) in enumerate(train_loader):

			if(enable_cuda):
				data= data.cuda()
				labels = labels.cuda()

			if(i==0):
				train_loss = 0.0
				
			output = model(data)
			# print('Label',labels)
			sup_loss,reg = loss_function(output, labels, model,weight_decay)
			loss = sup_loss
			
			# print statistics
			train_loss += loss.item()
			N = len(train_loader)
			if(i == N-1):
				if(verbose):
					print('[%d, %5d] Train loss: %.3f'%
						(epoch + 1, i + 1, train_loss / N ))

		for i, (data, labels) in enumerate(val_loader):

			if(enable_cuda):
				data= data.cuda()
				labels = labels.cuda()

			if(i==0):
				val_loss = 0.0
				
			output = model(data)
			sup_loss,reg = loss_function(output, labels, model,weight_decay)
			loss = sup_loss
			
			# print statistics
			val_loss += loss.item()
			N = len(val_loader)
			if(i == N-1):
				if(verbose):
					print('[%d, %5d] Validation loss: %.3f'%
						(epoch + 1, i + 1, val_loss / N ))

	return val_loss/N


#Perform 1 training step for joint EC-EO model
def train_merge(train_loader, model, loss_function, optimizer, epoch, weight_decay,gradacc,verbose):

	global enable_cuda

	model.train()

	for i, (data_EC, data_EO, labels) in enumerate(train_loader):

		if(enable_cuda):
			data_EC= data_EC.cuda()
			data_EO= data_EO.cuda()

			labels = labels.cuda()

		if(i==0):
			running_loss = 0.0
			running_reg = 0.0
			running_sup_loss = 0.0

		output = model(data_EC,data_EO)

		sup_loss,reg = loss_function(output, labels, model,weight_decay)
		sup_loss,reg=sup_loss/gradacc,reg/gradacc
		loss = sup_loss + reg

		loss.backward()

		#Perform gradient accumulation
		if((i+1)%gradacc ==0):
			optimizer.step()
			optimizer.zero_grad()

		# print statistics
		running_loss += loss.item()
		running_reg += reg.item()
		running_sup_loss += sup_loss.item()
		N = len(train_loader)
		if(i==N-1):
			if(verbose):
				print('[%d, %5d] loss: %.3f supervised loss: %.3f regularization loss %.3f'%
						(epoch + 1, i + 1, running_loss / N, running_sup_loss/N, running_reg/N))
			running_loss = 0.0
			running_reg = 0.0
			running_sup_loss = 0.0

#Perform validation for joint EC-EO model
def validate_merge(train_loader, val_loader,model,loss_function,epoch,weight_decay,verbose):

	global enable_cuda

	with torch.no_grad():
		model.eval()
		
		for i, (data_EC, data_EO, labels) in enumerate(train_loader):

			if(enable_cuda):
				data_EC= data_EC.cuda()
				data_EO= data_EO.cuda()
				labels = labels.cuda()

			if(i==0):
				train_loss = 0.0
				
			output = model(data_EC,data_EO)
			sup_loss,reg = loss_function(output, labels, model,weight_decay)
			loss = sup_loss
			
			# print statistics
			train_loss += loss.item()
			N = len(train_loader)
			if(i == N-1):
				if(verbose):
					print('[%d, %5d] Train loss: %.3f'%
						(epoch + 1, i + 1, train_loss / N ))

		for i, (data_EC,data_EO, labels) in enumerate(val_loader):

			if(enable_cuda):
				data_EC= data_EC.cuda()
				data_EO= data_EO.cuda()                
				labels = labels.cuda()

			if(i==0):
				val_loss = 0.0
				
			output = model(data_EC,data_EO)
			sup_loss,reg = loss_function(output, labels, model,weight_decay)
			loss = sup_loss
			
			# print statistics
			val_loss += loss.item()
			N = len(val_loader)
			if(i == N-1):
				if(verbose):
					print('[%d, %5d] Validation loss: %.3f'%
						(epoch + 1, i + 1, val_loss / N ))

	return val_loss/N


#Perform prediction on new test set
def predict(test_loader,model,verbose):

	global enable_cuda

	with torch.no_grad():
		model.eval()

		test_out_list=[]
		
		for i, (data_EC,data_EO) in enumerate(test_loader):

			if(enable_cuda):
				data_EC= data_EC.cuda()
				data_EO= data_EO.cuda()                

			test_out = model(data_EC,data_EO)
			test_out_list.append(test_out)


	test_out_list=torch.cat(test_out_list,dim=0)

	predictions=test_out.cpu().data.numpy()

	return predictions

if __name__ == '__main__':

	main()