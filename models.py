import numpy as np
import math
import torch
import torch.nn as nn
from torch.nn import Parameter
import torch.nn.functional as F


#Initalize model parameters
def init_weights(m):
	if (type(m) == nn.Linear or type(m) == nn.Conv2d or type(m) == nn.ConvTranspose2d):
		torch.nn.init.xavier_uniform_(m.weight)

#Model for separate EC/EO data
class MSFBCNN(nn.Module):
	def __init__(self,N,M,T,FT=4,FS=4):
		super(MSFBCNN, self).__init__()
		self.floatTensor = torch.FloatTensor if not torch.cuda.is_available() else torch.cuda.FloatTensor
		
		self.N=int(N)
		self.M=int(M)
		self.fs=100
		self.window_length=10.0
		self.T=int(self.fs*self.window_length)
		self.FT=FT
		self.FS=FS

		self.conv1a = nn.Conv2d(1, self.FT, (1, 33), padding = (0,16),bias=False)
		self.conv1b = nn.Conv2d(1, self.FT, (1, 21), padding = (0,10),bias=False)
		self.conv1c = nn.Conv2d(1, self.FT, (1, 13), padding = (0,6),bias=False)
		self.conv1d = nn.Conv2d(1, self.FT, (1, 9), padding = (0,4),bias=False)

		self.batchnorm1 = nn.BatchNorm2d(4*self.FT, False)
		
		# Spatial convolution
		self.conv2 = nn.Conv2d(4*self.FT, self.FS, (self.M,1),padding=(0,0),groups=1,bias=False)
		self.batchnorm2 = nn.BatchNorm2d(self.FS, False)

		self.pooling2 = nn.AvgPool2d(kernel_size=(1, 100),stride=(1,20),padding=(0,0))
	  
		self.drop=nn.Dropout(0.5)

		self.fc1=nn.Linear(FS*46,64)
		self.fc2=nn.Linear(64,1)
	
		self.layers=self.create_layers_field()
		self.apply(init_weights)
		

	def forward(self, x_in,mergemode=False):
				
		output_list=[]
		feat_list=[]

		Tstartlist=[0,250,500,750]

		if(self.training):
			num_it=1
		else:
			num_it=len(Tstartlist)
		for it in range(num_it):

			signal_length=x_in.size(-1)
			Tstart_max=signal_length-self.T

			Tstart=torch.randint(0,Tstart_max+1,(1,))[0]

			if(self.training):
				x=x_in[:,:,:,Tstart:Tstart+self.T]
			else:
				x=x_in[:,:,:,Tstartlist[it]:Tstartlist[it]+self.T]

			if(self.training):
				x=x+0.001*torch.randn(x.size()).cuda()
			
			# Layer 1
			x1 = self.conv1a(x)
			x2 = self.conv1b(x)
			x3 = self.conv1c(x)
			x4 = self.conv1d(x)
			
			x = torch.cat([x1,x2,x3,x4],dim=1)
			x = self.batchnorm1(x)

			# Layer 2
			x = torch.pow(self.batchnorm2(self.conv2(x)),2)
			x = self.pooling2(x)
			x = torch.log(x)
			
			x=torch.flatten(x,start_dim=1)
			x = self.drop(x)
			feat=self.fc1(x)
			x=self.fc2(F.relu(feat))			
		   

			output_list.append(x)
			feat_list.append(feat)

		out=torch.stack(output_list,dim=2)
		out=torch.mean(out,dim=-1)

		feat=torch.stack(feat_list,dim=2)
		feat=torch.mean(feat,dim=-1)

		if(mergemode):
			return out,feat
		else:      
			return out

	def regularizer(self,weight_decay):

		#L2-Regularization of other layers
		reg=self.floatTensor([0])
		for i,layer in enumerate(self.layers):
			if(type(layer) == nn.Conv2d or type(layer) == nn.Linear):
				reg+=torch.sum(torch.pow(layer.weight,2))
		reg = weight_decay*reg
		return reg

	def create_layers_field(self):
		layers = []
		for idx, m in enumerate(self.modules()):
			if(type(m) == nn.Conv2d or type(m) == nn.Linear):
				layers.append(m)
		return layers
	
#Model combining EC and EO model
class Merge_MSFBCNN(nn.Module):
	def __init__(self,N,M,T,FT=4,FS=4):
		super(Merge_MSFBCNN, self).__init__()
		self.floatTensor = torch.FloatTensor if not torch.cuda.is_available() else torch.cuda.FloatTensor

		self.model1=MSFBCNN(N,M,T,FT=FT,FS=FS)
		self.model2=MSFBCNN(N,M,T,FT=FT,FS=FS)

		self.fc1=nn.Linear(128,20)
		self.fc2=nn.Linear(20,2)

		self.layers=self.create_layers_field()
		self.apply(init_weights)
		

	def forward(self, x_in_EC, x_in_EO):

		out1,feat1=self.model1(x_in_EC,mergemode=True)
		out2,feat2=self.model2(x_in_EO,mergemode=True)

		out=torch.cat((out1,out2),dim=-1)
		feat=torch.cat((feat1,feat2),dim=-1)

		attention=F.softmax(self.fc2(F.relu(self.fc1(feat))),dim=-1)
		out=torch.sum(attention*out,dim=-1,keepdim=True)
				
		return out

	def regularizer(self,weight_decay):

		#L2-Regularization of other layers
		reg=self.floatTensor([0])
		for i,layer in enumerate(self.layers):
			if(type(layer) == nn.Conv2d or type(layer) == nn.Linear):
				reg+=torch.sum(torch.pow(layer.weight,2))
		reg = weight_decay*reg
		return reg

	def create_layers_field(self):
		layers = []
		for idx, m in enumerate(self.modules()):
			if(type(m) == nn.Conv2d or type(m) == nn.Linear):
				layers.append(m)
		return layers

