import mne
import numpy as np
import math
import torch
import torch.nn as nn
from torch.nn import Parameter
import torch.nn.functional as F
import os
import pandas as pd

#Split data in training/validation/test data
#Data is split in such a way that training and validation set have similar histograms
def split_data(train_split,val_split,labels,randomseed):
	
	num_subjects=1200
	subjects=np.arange(num_subjects)
	np.random.seed(randomseed)
	np.random.shuffle(subjects)
	split1=round(num_subjects*train_split)
	split2=round(num_subjects*(train_split+val_split))
	train_idx=subjects[:split1]
	val_idx=subjects[split1:split2]

	return train_idx,val_idx

#Data loader for separate EC and EO data
def EC_EO_loader(train_path,train_split,val_split,batch_size=64,randomseed=4):

	condition = "EC"
	num_subjects = 1200

	labels = pd.read_csv(train_path + "train_subjects.csv")["age"]
	labels=list(labels[:])

	train_idx,val_idx=split_data(train_split,val_split,labels,randomseed)

	tr_ds=[]
	val_ds=[]
	
	for s in range(1, num_subjects + 1):
		fname = f"subj{s:04}_{condition}_raw.fif.gz"
		raw = mne.io.read_raw(train_path + fname, preload=True,verbose='error')
		if(raw.info['sfreq']!=500):
			raw.resample(sfreq=500)
		raw.notch_filter([60.0, 120.0, 180.0],verbose='error').filter(l_freq=2, h_freq=50,verbose='error')
		raw.resample(sfreq=100)
		# Marking some channels as bad (flat electrodes, disconnection, noisy signal, ...)
		raw.info["bads"] = [
			"E7",
			"E48",
			"E106",
			"E112",
			"E55",
			"E31",
			"E105",
			"Cz",
			"E80",
			"E81",
			"E88",
			"E99",
			"E67"
		]
		raw.drop_channels(raw.info["bads"])
		epochs=mne.make_fixed_length_epochs(raw,38.0,overlap=0.0,preload=True,verbose='error')
		x=epochs.get_data()
		x=torch.Tensor(x)
		x=x[[0],:,:]


		m=torch.mean(x,dim=(-1),keepdim=True)
		st=torch.std(x,dim=(-1),keepdim=True)
		x=(x-m)/st
		
		y=torch.Tensor(np.array([labels[s-1]]))

		if s-1 in train_idx:
			tr_ds.append([x,y])
		elif s-1 in val_idx:
			val_ds.append([x,y])
		else:
			pass


	datasize=x.size()
		
	trainloader_EC=torch.utils.data.DataLoader(tr_ds,batch_size=batch_size,shuffle=True)
	valloader_EC=torch.utils.data.DataLoader(val_ds,batch_size=batch_size,shuffle=False)

	condition = "EO"

	tr_ds=[]
	val_ds=[]

	for s in range(1, num_subjects + 1):
		fname = f"subj{s:04}_{condition}_raw.fif.gz"
		raw = mne.io.read_raw(train_path + fname, preload=True,verbose='error')
		if(raw.info['sfreq']!=500):
			raw.resample(sfreq=500)
		raw.notch_filter([60.0, 120.0, 180.0],verbose='error').filter(l_freq=2, h_freq=50,verbose='error')
		raw.resample(sfreq=100)
		# Marking some channels as bad (flat electrodes, disconnection, noisy signal, ...)
		raw.info["bads"] = [
			"E7",
			"E48",
			"E106",
			"E112",
			"E55",
			"E31",
			"E105",
			"Cz",
			"E80",
			"E81",
			"E88",
			"E99",
			"E67"
		]
		raw.drop_channels(raw.info["bads"])
		epochs=mne.make_fixed_length_epochs(raw,19.0,overlap=0.0,preload=True,verbose='error')
		x=epochs.get_data()
		x=torch.Tensor(x)
		x=x[[0],:,:]


		m=torch.mean(x,dim=(-1),keepdim=True)
		st=torch.std(x,dim=(-1),keepdim=True)
		x=(x-m)/st
		
		y=torch.Tensor(np.array([labels[s-1]]))

		if s-1 in train_idx:
			tr_ds.append([x,y])
		elif s-1 in val_idx:
			val_ds.append([x,y])
		else:
			pass



	datasize=x.size()
		
	trainloader_EO=torch.utils.data.DataLoader(tr_ds,batch_size=batch_size,shuffle=True)
	valloader_EO=torch.utils.data.DataLoader(val_ds,batch_size=batch_size,shuffle=False)

	return trainloader_EC,valloader_EC,trainloader_EO,valloader_EO,datasize

#Data loader for joint EC and EO data
def joint_EC_EO_loader(train_path,train_split,val_split,batch_size=64,randomseed=4):

	conditions = ["EC","EO"]
	num_subjects = 1200

	labels = pd.read_csv(train_path + "train_subjects.csv")["age"]
	labels=list(labels)

	train_idx,val_idx=split_data(train_split,val_split,labels,randomseed)

	tr_ds=[]
	val_ds=[]
	for s in range(1, num_subjects + 1):            
		xs=[]
		for c in conditions:
			fname = f"subj{s:04}_{c}_raw.fif.gz"
			raw = mne.io.read_raw(train_path + fname, preload=True,verbose='error')
			if(raw.info['sfreq']!=500):
				raw.resample(sfreq=500)
			raw.notch_filter([60.0, 120.0, 180.0],verbose='error').filter(l_freq=2, h_freq=50,verbose='error')
			raw.resample(sfreq=100)
			# Marking some channels as bad (flat electrodes, disconnection, noisy signal, ...)
			raw.info["bads"] = [
				"E7",
				"E48",
				"E106",
				"E112",
				"E55",
				"E31",
				"E105",
				"Cz",
				"E80",
				"E81",
				"E88",
				"E99",
				"E67"
			]
			raw.drop_channels(raw.info["bads"])
			if(c=="EC"):
				epochs=mne.make_fixed_length_epochs(raw,38.0,overlap=0.0,preload=True,verbose='error')
			else:
				epochs=mne.make_fixed_length_epochs(raw,19.0,overlap=0.0,preload=True,verbose='error')
			x=epochs.get_data()
			x=torch.Tensor(x)
			x=x[[0],:,:]

			m=torch.mean(x,dim=(-1),keepdim=True)
			st=torch.std(x,dim=(-1),keepdim=True)
			x=(x-m)/st
				

			xs.append(x)

		y=torch.Tensor(np.array([labels[s-1]]))

		if s-1 in train_idx:
			tr_ds.append([xs[0],xs[1],y])
		elif s-1 in val_idx:
			val_ds.append([xs[0],xs[1],y])
		else:
			pass



	datasize=x.size()
		
	trainloader=torch.utils.data.DataLoader(tr_ds,batch_size=batch_size,shuffle=True)
	valloader=torch.utils.data.DataLoader(val_ds,batch_size=batch_size,shuffle=False)

	return trainloader,valloader,datasize

#Data loader for unknown test set
def predictionloader(prediction_path,batch_size=4):

	conditions = ["EC","EO"]
	num_subjects = 400

	test_ds=[]
	for s in range(1601, num_subjects + 1601):            
		xs=[]
		for c in conditions:
			fname = f"subj{s:04}_{c}_raw.fif.gz"
			raw = mne.io.read_raw(prediction_path + fname, preload=True,verbose='error')
			if(raw.info['sfreq']!=500):
				raw.resample(sfreq=500)
			raw.notch_filter([60.0, 120.0, 180.0],verbose='error').filter(l_freq=2, h_freq=50,verbose='error')
			raw.resample(sfreq=100)
			# Marking some channels as bad (flat electrodes, disconnection, noisy signal, ...)
			raw.info["bads"] = [
				"E7",
				"E48",
				"E106",
				"E112",
				"E55",
				"E31",
				"E105",
				"Cz",
				"E80",
				"E81",
				"E88",
			]
			raw.drop_channels(raw.info["bads"])
			if(c=="EC"):
				epochs=mne.make_fixed_length_epochs(raw,38.0,overlap=0.0,preload=True,verbose='error')
			else:
				epochs=mne.make_fixed_length_epochs(raw,19.0,overlap=0.0,preload=True,verbose='error')
			x=epochs.get_data()
			x=torch.Tensor(x)
			x=x[[0],:,:]

			#Remove channels with large mean/std
			sel=np.arange(x.size(-2))
			sel=torch.LongTensor(np.delete(sel,[91,62]))
			
			x=torch.index_select(x,dim=-2,index=sel)

			m=torch.mean(x,dim=(-1),keepdim=True)
			st=torch.std(x,dim=(-1),keepdim=True)
			x=(x-m)/st
				

			xs.append(x)

		test_ds.append([xs[0],xs[1]])



	datasize=x.size()

	print(len(test_ds[0]))
		
	testloader=torch.utils.data.DataLoader(test_ds,batch_size=batch_size,shuffle=False)

	return testloader